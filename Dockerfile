FROM golang:1.9-alpine

RUN apk update
RUN apk add git

WORKDIR /go/src/
RUN go get golang.org/x/tour/gotour

CMD ["gotour"]

package main

import (
	"fmt"
)

// type assertion provides access to interface-value's underlying value
func main() {
	var i interface{} = "Hello"

	s := i.(string)
	fmt.Println(s)

	// test whether an interface value holds a specific type
	s, ok := i.(string)
	fmt.Println(s, ok)

	f, ok := i.(float64)
	fmt.Println(f, ok)

	// panic,
	// as type assertion here only has one return value
	f = i.(float64)
}

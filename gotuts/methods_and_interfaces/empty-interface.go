package main

import "fmt"

// create an empty interface
type Itf interface{}

func main() {
	// Alternative way to create an empty interface
	var i interface{}
	describe(i)

	var itf Itf
	describe(itf)

	// An empty interface may hold values of any type.
	// Empty interfaces are used by code that handles values of unknown type.
	// For example, fmt.Print takes any number of arguments of type interface{}.
	i = 42
	describe(i)

	itf = "hello"
	describe(itf)
}

// To allow describe to accept any type as the argument,
// empty-interface is specified as argument
func describe(i interface{}) {
	fmt.Printf("(%v, %T)\n", i, i)
}

package main

import (
	"fmt"
	"math"
)

// With function arguments, when *T (pointer to type T) has been specified,
// you must provide a pointer to that type as parameter.
// Same goes, if the function-argument has specified a value argument,
// one must provide a value and not a pointer

// However, with method and receivers, it would implicitly turn a value to pointer OR
// turn a pointer to value

// There are two reasons to use a pointer receiver.
// The first is so that the method can modify the value that its receiver points to.
// The second is to avoid copying the value on each method call. This can be more efficient if the receiver is a large struct, for example.
// In general, all methods on a given type should have either value or pointer receivers, but not a mixture of both.

type Vertex struct {
	X, Y float64
}

func (v *Vertex) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func ScaleFunc(v *Vertex, f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func AbsFunc(v Vertex) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	v := Vertex{3, 4}
	p := &v
	// Scale will automatically turn v into a pointer
	// p.Scale(2) will work too
	v.Scale(2)
	fmt.Println(v)

	// With function arguments, we must provide a pointer if that was the definition
	ScaleFunc(p, 2)
	fmt.Println(v)
	// Providing value where pointer was defined, it will go boom
	// ScaleFunc(v, 2)

	// p.Abs() will work. Behind the scenes it is interpreted as (*p).Abs()
	fmt.Println(p.Abs())

	fmt.Println(AbsFunc(v))
	// Providing pointer where value was defined, it will go boom
	// fmt.Println(AbsFunc(p))
	// But we can provide it with the value of our pointer
	fmt.Println(AbsFunc(*p))
}

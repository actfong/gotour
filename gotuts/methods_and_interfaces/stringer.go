package main

import "fmt"

// A Stringer is a type that can describe itself as a string.
// The fmt package (and many others) look for this interface to print values.

// type Stringer interface {
//     String() string
// }

type Person struct {
	Name string
	Age  int
}

func (p Person) String() string {
	return fmt.Sprintf("Name: %v, Age: %v", p.Name, p.Age)
}

func main() {
	donald := &Person{"donald", 80}
	vladimir := &Person{"vladimir", 70}
	fmt.Println(donald, vladimir)
	// If Person didn't implement String(), output would have been:
	// {donald 80} {vladimir 70}
}

package main

import (
	"fmt"
	"math"
)

type ErrNegativeSqrt float64

func (e ErrNegativeSqrt) Error() string {
	// fmt.Sprintf(e) will call e.Error() to convert the value e to a string.
	// If the Error() method calls fmt.Sprint(e), then the program recurses until out of memory.

	// create a new value
	newE := float64(e)
	return fmt.Sprintf("cannot Sqrt negative number: %v", newE)
}

func Sqrt(x float64) (float64, error) {
	if x < 0 {
		neg := ErrNegativeSqrt(x)
		return 0, neg
	}
	return math.Sqrt(x), nil
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))
}

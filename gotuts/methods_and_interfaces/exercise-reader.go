package main

import (
	"golang.org/x/tour/reader"
)

type MyReader struct{}

func (mr MyReader) Read(byteSlice []byte) (int, error) {
	for i := range byteSlice {
		byteSlice[i] = 'A'
	}
	return len(byteSlice), nil
}

func main() {
	reader.Validate(MyReader{})
}

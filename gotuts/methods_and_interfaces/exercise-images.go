package main

import "golang.org/x/tour/pic"
import "image/color"
import "image"

type MyImage struct{}

func (mi MyImage) ColorModel() color.Model {
	return color.RGBAModel
}

func (mi MyImage) Bounds() image.Rectangle {
	return image.Rect(24, 48, 30, 60)
}

func (mi MyImage) At(x, y int) color.Color {
	return color.RGBA{0, 0, 0, 0}
}

func main() {
	m := MyImage{}
	pic.ShowImage(m)
}

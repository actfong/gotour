package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	reader io.Reader
}

func (r13R rot13Reader) Read(byteSlice []byte) (int, error) {
	output, err := r13R.reader.Read(byteSlice)
	for i, v := range byteSlice {
		fmt.Printf("i: %v, v: %v\n", i, v)
		byteSlice[i] = rot13(byteSlice[i])
	}
	return output, err
}

func rot13(b byte) byte {
	var a, z byte
	switch {
	case 'a' <= b && b <= 'z':
		a, z = 'a', 'z'
	case 'A' <= b && b <= 'Z':
		a, z = 'A', 'Z'
	default:
		return b
	}
	return (b-a+13)%(z-a+1) + a
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}

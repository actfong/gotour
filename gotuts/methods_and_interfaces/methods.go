package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

// Methods are functions, but with a special receiver argument
// With this receiver argument, we state clearly what Type the receiver should have

// You can only declare a method with a receiver whose type is defined in the same package as the method.
// You cannot declare a method with a receiver whose type is defined in another package (which includes the built-in types such as int).
func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// It can also be rewritten back to a plain function
// In this case, the receiver is passed in as an argument
func AbsFunc(v Vertex) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	v := Vertex{3, 4}
	fmt.Println(v.Abs())
	fmt.Println(AbsFunc(v))
}

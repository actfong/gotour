package main

import (
	"fmt"
	"io"
	"strings"
)

func main() {
	reader := strings.NewReader("Hello, Monsieur 1234!")
	fmt.Printf("READER => Type: %T, Value: %v\n", reader, reader)

	byteSlice := make([]byte, 8)
	fmt.Printf("BYTESLICE => Type: %T, Value: %v\n\n", byteSlice, byteSlice)

	for {
		output, err := reader.Read(byteSlice)

		fmt.Printf("type of Read() output is %T\n", output)
		fmt.Printf("output = %v err = %v byteSlice = %v\n", output, err, byteSlice)
		fmt.Printf("byteSlice[:output] = %q\n\n", byteSlice[:output])
		if err == io.EOF {
			break
		}
	}
}

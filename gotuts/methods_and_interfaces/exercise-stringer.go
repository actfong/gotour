package main

import "fmt"

type IPAddr [4]byte

// TODO: Add a "String() string" method to IPAddr.
func (ipa IPAddr) String() string {
	return fmt.Sprintf("%v.%v.%v.%v\n", ipa[0], ipa[1], ipa[2], ipa[3])
}

func main() {
	hosts := map[string]IPAddr{
		"loopback":  {127, 0, 0, 1},
		"googleDNS": {8, 8, 8, 8},
	}
	for name, ip := range hosts {
		fmt.Printf("%v: %v\n", name, ip)
	}

	newHosts := map[string]IPAddr{
		"localhost": {127, 0, 0, 1},
		"docker":    {192, 168, 0, 99},
	}
	for _, ipa := range newHosts {
		fmt.Print(ipa)
	}
}

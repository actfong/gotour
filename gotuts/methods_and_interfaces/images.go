package main

import (
	"fmt"
	"image"
)

// package image
//
// type Image interface {
//     ColorModel() color.Model
//     Bounds() Rectangle
//     At(x, y int) color.Color
// }

func main() {
	img := image.NewRGBA(image.Rect(0, 0, 100, 100))
	// fmt.Printf("Type: %T, Value %v", img, img)
	fmt.Println(img.Bounds)
	fmt.Println(img.At(100, 100).RGBA())
}

package main

import (
	"fmt"
)

type Walkable interface {
	Run() string
	Sprint() string
}

type Talkable interface {
	Talk() string
}

type Person struct {
	name string
}

// implement the method for object receiver of type Person
func (p Person) Run() string {
	return ("A person is running")
}

func (p Person) Sprint() string {
	return ("A person is sprinting")
}

func (p *Person) Talk() string {
	return ("Talking....")
}

func main() {
	donald := Person{"donald"}
	var w Walkable
	// Tell Interface value "w" to hold a value of type Person, which implements it methods
	w = donald

	// Run() method expects a value receiver, so all is good
	fmt.Printf("(%v, %T)\n", w, w)
	fmt.Println(w)
	fmt.Println(w.Run())

	// Tell interface-value "w" to hold a pointer receiver to a Person.
	w = &donald
	// Run() method expect a value receiver, but will still work when a pointer is the receiver
	fmt.Printf("(%v, %T)\n", w, w)
	fmt.Println(w)
	fmt.Println(w.Run())

	// Tell Interface value "w" to hold a value of type Person, which implements it methods
	// Pay attention to an alternative way of assigning an interface value
	// var t Talkable = Person{"vladimir"}

	// Interface value can also hold a nil-value,
	// as long as the Type of this nil-value implements this interface.
	var zero Person
	w = zero
	fmt.Println(w)
	fmt.Println(w.Run())

	// a "nil interface value" is when an interface-value has no value nor type
	var nul Walkable
	fmt.Println(nul)
	// invoking a method on a nil-interface-value will trigger a RuntimeError
	// fmt.Println(nul.Run())
}

// An interface value holds a value of a specific underlying concrete type.
// Calling a method on an interface value executes the method of the same name on its underlying type.

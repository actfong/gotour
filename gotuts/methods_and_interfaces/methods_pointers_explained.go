package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

// rewrite Abs() as a function
func Abs(v Vertex) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// rewrite Scale() as a function
// If we remove the pointer in func-arg, then we must remove & in the invocation to Scale()
func Scale(v *Vertex, f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func main() {
	v := Vertex{3, 4}
	fmt.Println(Abs(v))
	Scale(&v, 2)
	fmt.Println(Abs(v))
}

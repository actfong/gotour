package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

// value receiver:
// It will create a copy of the receiver,
// as a result nothing will change on the original receiver (v)
func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// pointer receiver:
// When this is not a pointer receiver, the values of v won't be changed

// Methods with pointer receivers can modify the value to which the receiver points.
// Since methods often need to modify their receiver,
// pointer receivers are more common than value receivers.

// Try remove the * and see what happens
func (v *Vertex) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func main() {
	v := Vertex{3, 4}
	fmt.Println(v.Abs())
	v.Scale(2)
	fmt.Println(v.Abs())
}

package main

import (
	"fmt"
)

func main() {
	// Provide the buffer length as the second argument to make to initialize a buffered channel
	chn := make(chan int, 2)
	// Sends to a buffered channel block only when the buffer is full.
	chn <- 1
	chn <- 2

	// Try to send an extra value to channel. Boom!
	// chn <- 3
	// Before sending extra value, we have to receive from channel first to free space

	// Receives block when the buffer is empty.
	fmt.Println(<-chn)
	fmt.Println(<-chn)
}

package main

import (
	"fmt"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println("%v %v", s, i)
	}
}

func main() {
	// Here `say("World")` is blocking,
	// hence this program will force `go say("Hello")` to complete.

	// Try to run `say("World")` in a Go routine and `say("Hello")` in blocking mode,
	// and see what happens
	go say("Hello")
	say("World")
}

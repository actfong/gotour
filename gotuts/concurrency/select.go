package main

import (
	"fmt"
	"time"
)

func main() {
	c := make(chan int)
	quit := make(chan int)

	// run anonymous function to send values to quit
	go func() {
		for i := 0; i < 10; i++ {
			// receive from c
			fmt.Println(<-c)
			if i == 5 {
				quit <- 0
			}
		}
		quit <- 0
	}()
	fibonacci(c, quit)
}

func fibonacci(c, quit chan int) {
	x, y := 0, 1
	for {
		select {
		case c <- x:
			x, y = y, x+y
		// case <- quit is blocked until someone sends it something
		case <-quit:
			fmt.Println("receive quit")
		default:
			fmt.Println("    .")
			time.Sleep(50 * time.Millisecond)
			if x >= 34 {
				fmt.Println("x: %v", x)
				fmt.Println("quitting for real")
				return
			}
		}
	}
}

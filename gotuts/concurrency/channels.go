package main

import (
	"fmt"
)

// Send and Receive values with the Channel operator <-
// ch <- v    	// Send v to channel ch.
// v := <-ch  	// Receive from ch, and
// assign value to v.

// With Channels, by Default, sending and receiving block until the other side is ready.
// This allows goroutines to synchronize without explicit locks or condition variables.
func sum(s []int, c chan int) {
	sum := 0
	for _, v := range s {
		sum += v
	}
	c <- sum // send sum to c
	fmt.Printf("done with summing %v\n", s)
}

func main() {
	s := []int{7, 2, 8, -9, 4, 0}

	// Like maps and slices, channels must be created before use!
	c := make(chan int)

	// first half
	go sum(s[:len(s)/2], c)

	// second half
	go sum(s[len(s)/2:], c)

	// sum the whole thing
	go sum(s, c)

	// The order in which the channel returns its values is random
	x, y, z := <-c, <-c, <-c // receive from c

	fmt.Println(x, y, z, x+y+z)
}

package main

import (
	"fmt"
	"time"
)

// 1. Channel closing
// A sender can close a channel to indicate that no more values will be sent.
// Receivers can test whether a channel has been closed by assigning a second parameter to the receive expression

// `v, ok := <-ch``

// ok is `false`` if there is nothing more to receive
// Once a channel is closed, sending on that channel will cause a panic.
// Only the sender should close a channel, never the receiver
// Channels aren't like files; you don't usually need to close them.
// Closing is only necessary when the receiver must be told there are no more values coming, such as to terminate a range loop.

// 2. range() on channels
// `for i, _v := range c``
// receives values from the channel repeatedly until it is closed.

// func fibonacci is the sender
func fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		time.Sleep(1 * time.Second)
		fmt.Println("waiting in Fibo")
		c <- x
		x, y = y, x+y
	}

	fmt.Println("closing.....................")
	// close the channel
	close(c)
}

func printNums(c chan int) {
	for i := range c {
		time.Sleep(1 * time.Second)
		fmt.Println("waiting in PrintNum")
		fmt.Println(i)
	}
	v, ok := <-c
	fmt.Printf("v, ok = %v, %v\n", v, ok)
}

func main() {
	c := make(chan int, 10)
	go fibonacci(cap(c), c)
	printNums(c)
}

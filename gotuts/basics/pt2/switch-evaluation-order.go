package main

import (
	"fmt"
	"time"
)

func daysFrom(weekday time.Weekday, days int) int {
	const DaysInWeek = 7

	intWeekday := int(weekday)
	if int(weekday)+days < DaysInWeek {
		return intWeekday + days
	} else {
		return intWeekday + days - DaysInWeek
	}
}

// case statements can also take expressions instead of static values
func whenIs(weekday time.Weekday) {
	fmt.Printf("When is %s?\n", weekday)
	today := time.Now().Weekday()
	switch int(weekday) {
	case daysFrom(today, 0):
		fmt.Println("today")
	case daysFrom(today, 1):
		fmt.Println("tomorrow")
	case daysFrom(today, 2):
		fmt.Println("in two days")
	default:
		fmt.Println("Too far away")
	}
}

func main() {
	whenIs(time.Sunday)
	whenIs(time.Monday)
	whenIs(time.Tuesday)
	whenIs(time.Friday)
}

package main

import (
	"fmt"
	"time"
)

// Switch without a condition is the same as switch true.
// This construct can be a clean way to write long if-then-else chains.
func main() {
	t := time.Now()
	fmt.Println(t)

	switch {
	case t.Hour() < 12:
		fmt.Println("Good morning")
	case t.Hour() < 18:
		fmt.Println("Good afternoon")
	case t.Hour() >= 18:
		fmt.Println("Good evening")
	}
}

package main

import "fmt"

// A defer statement defers the execution of a function until the surrounding function returns.
func simpleDefer() {
	defer fmt.Println("Hello")
	fmt.Println("world")
}

// defer is being stacked, hence LIFO
func stackedDefer() {
	fmt.Println("counting...")
	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}
	fmt.Println("done.")
}
func main() {
	simpleDefer()
	stackedDefer()
}

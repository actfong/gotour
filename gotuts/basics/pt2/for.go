package main

import "fmt"

// Standard for-loop with init-statement, condition expression and post-statement
func forWithInitAndPost() {
	sum := 0
	for i := 0; i < 10; i++ {
		sum += i
		fmt.Println("i is", i)
	}
	fmt.Println("Sum is", sum)
}

// init- and post-statements are optional
// when they are dropped, it acts like a while-loop
// can also take the notation `; sum < 10 ;``
func forWithoutInitAndPost() {
	sum := 1
	for sum < 10 {
		sum += sum
		fmt.Println("Intermediate Sum is", sum)
	}
	fmt.Println("Final Sum is", sum)
}

// without the condition expression, you create an infinite loop
func infiniteLoop() {
	sum := 1
	for {
		sum += sum
		fmt.Println("sum = ", sum)
		if sum > 100 {
			fmt.Println("breaking....")
			break
		}
	}
}

func main() {
	forWithInitAndPost()
	forWithoutInitAndPost()
	infiniteLoop()
}

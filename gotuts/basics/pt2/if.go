package main

import (
	"fmt"
	"math"
)

// For if-statements, no () are required around the condition-expression
func sqrt(x float64) string {
	if x < 0 {
		return sqrt(-x) + "i"
	}
	return fmt.Sprint(math.Sqrt(x))
}

// if-statements can start with a short-statement
// with variable-assignments, these variables are visible within the scope of if/else
func pow(x, y, lim float64) float64 {
	if v := math.Pow(x, y); v < lim {
		fmt.Println(v)
	} else {
		fmt.Printf("result of %d ** %d = %d\n", int(x), int(y), int(v))
	}
	return lim
}

func main() {
	fmt.Println(sqrt(-3), sqrt(3), sqrt(2), sqrt(4))
	fmt.Println(pow(2, 3, 10))
	fmt.Println(pow(2, 3, 6))
}

package main

import (
	"fmt"
	"runtime"
)

// switch case default
// switch-statements (like if-statements) can take a short-statement at the start
func main() {
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Print("MacOS X.")
	case "linux":
		fmt.Print("Linux.")
	default:
		fmt.Print("%s.", os)
	}
}

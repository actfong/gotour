package main

import (
	"fmt"
)

type City struct {
	Name      string
	Lat, Long float64
}

func main() {
	berlin := City{"Berlin", 3, 5}
	hamburg := City{"Hamburg", 2, 4}
	munich := City{"München", 6, 8}

	// The make function returns a map of the given type, initialized and ready for use.
	var cityMap = make(map[string]City)
	cityMap["Berlin"] = berlin
	cityMap["hamburg"] = hamburg
	cityMap["munich"] = munich
	fmt.Println(cityMap)

	// create a nil map
	// A nil map has no keys, nor can keys be added.
	var m map[string]City
	// exception: assignment to entry in nil map
	m["Berlin"] = berlin
	fmt.Println(m)
}

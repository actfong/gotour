package main

import "fmt"

func main() {
	var primes = []int{2, 3, 5, 7, 11, 14}

	s := primes[:]
	fmt.Println(s)

	s = primes[1:4]
	fmt.Println(s)

	s = s[:2]
	fmt.Println(s)

	s = s[1:]
	fmt.Println(s)
}

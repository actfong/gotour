package main

import (
	"fmt"
)

func main() {
	var s []int

	s = append(s, 3)

	s = append(s, 2, 3, 4)
	fmt.Println(s, len(s), cap(s))

	evens := []int{2, 4, 6, 8, 10}
	newSlice := evens[:]
	fmt.Println(newSlice)
	fmt.Printf("new slice has value: %v\n", newSlice)
	fmt.Printf("new slice has capacity: %v length: %v\n", cap(newSlice), len(newSlice))

	// If the backing array of s is too small to fit all the given values a bigger array will be allocated.
	// The returned slice will point to the newly allocated array.
	newSlice = append(newSlice, 12)
	fmt.Printf("new slice has value: %v\n", newSlice)
	fmt.Printf("new slice has capacity: %v length: %v\n", cap(newSlice), len(newSlice))
}

package main

import (
	"fmt"
)

type Vertex struct {
	X int
	Y int
}

var (
	v1 = Vertex{24, 42}
	// Y defaults to 0 (nil value)
	v2 = Vertex{X: 24}
	v3 = Vertex{}

	v5 = Vertex{Y: 42, X: 24}
	// pointer to a struct value
	p = &Vertex{1, 2}
)

func main() {
	fmt.Println(v1, v2, v3, p)
	fmt.Println(v5.X)
	fmt.Println(v5.Y)
}

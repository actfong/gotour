package main

import "fmt"

type Vertex struct {
	X int
	Y int
}

type Foo struct {
	x int
	y int
}

func main() {
	// create a Struct
	myVertex := Vertex{1, 2}
	fmt.Println(myVertex)

	// set values for Struct-fields
	myVertex.Y = 19
	fmt.Println(myVertex.Y)

	foo := Foo{24, 42}
	fmt.Println(foo.x)
	fmt.Println(foo.y)
	fmt.Printf("%v\n", foo.y)

	// create a pointer to foo
	p := &foo
	// read struct-field through a pointer
	fmt.Println(*p)
	// set struct-field through a pointer
	p.x = 11
	fmt.Println(*p)
}

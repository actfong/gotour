package main

import "fmt"

func main() {
	var m = make(map[string]int)

	m["Question"] = 11
	m["Answer"] = 42
	fmt.Println(m)

	m["Answer"] = 48
	fmt.Println(m)

	delete(m, "Answer")
	fmt.Println(m)

	// If key is not in the map, then elem is the zero value for the map's element type.
	v, ok := m["Answer"]
	fmt.Printf("v = %v, ok = %v\n", v, ok)

	v, ok = m["Question"]
	fmt.Printf("v = %v, ok = %v\n", v, ok)
}

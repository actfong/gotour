package main

import (
	"fmt"
)

func main() {
	names := [4]string{
		"John", "Paul", "George", "Ringo",
	}
	fmt.Println(names)
	a := names[0:2]
	b := names[1:3]
	fmt.Println(a, b)

	// slices do not store data, they reference to underlying arrays
	// when modifying a slice, it modifies the underlying array.
	// As a result, other slices referring to the same elements in that array will see the changes
	a[1] = "Paulo"
	fmt.Println(a, b)
}

package main

import (
	"fmt"
	"strings"

	"golang.org/x/tour/wc"
)

func WordLength(s string) map[string]int {
	var m = make(map[string]int)

	arr := strings.Fields(s)

	for i := 0; i < len(arr); i++ {
		m[arr[i]] = len(arr[i])
	}
	fmt.Println(m)
	return m
}

func WordCount(s string) map[string]int {
	var m = make(map[string]int)
	arr := strings.Fields(s)

	// for-implementation
	// for i := 0; i < len(arr); i++ {
	// 	_, ok := m[arr[i]]
	// 	if ok {
	// 		m[arr[i]] += 1
	// 	} else {
	// 		m[arr[i]] = 1
	// 	}
	// }

	// range implementation
	for _, word := range arr {
		_, ok := m[word]
		if ok {
			m[word] = m[word] + 1
		} else {
			m[word] = 1
		}
	}
	return m
}

func main() {
	wc.Test(WordCount)
	WordLength("I ate a donut. Then I ate another donut.")
}

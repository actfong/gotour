package main

import (
	"fmt"
	"math"
)

func compute(fn func(float64, float64) float64) float64 {
	return fn(3, 4)
}

func main() {
	hyp := func(x, y float64) float64 {
		return math.Sqrt(x*x + y*y)
	}

	fmt.Println(hyp(3, 4))
	fmt.Println(compute(hyp))
	fmt.Println(compute(math.Pow))

}

package main

import (
	"fmt"
)

func main() {
	// slice-literal of ints. Length need not to be specified
	var ints = []int{1, 3, 5, 7, 9}
	fmt.Println(ints)

	// slice-literal of bools
	var bools = []bool{true, true, false, false}
	fmt.Println(bools)

	// slice-literal of structs that you define on the spot
	var structs = []struct {
		i int
		b bool
	}{
		{2, true},
		{3, false},
		{4, true},
		{5, false},
	}

	fmt.Println(structs)
}

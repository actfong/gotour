package main

import "fmt"

func fibonacci() func(int) int {
	// By definition, the first two numbers in the Fibonacci sequence are either 1 and 1, or 0 and 1,
	// depending on the chosen starting point of the sequence, and each subsequent number is the sum of the previous two.

	// In our case, we have chosen the first two numbers to be 0 and 1
	a := make([]int, 0)
	a = append(a, 0, 1)

	return func(i int) int {
		var num int
		if i < 2 {
			num = a[i]
		} else {
			a = append(a, a[len(a)-1]+a[len(a)-2])
			// a's length increased by 1
			num = a[len(a)-1]
		}
		return num
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f(i))
	}
}

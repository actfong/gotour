package main

import (
	"fmt"
)

func main() {
	i, j := 42, 2701

	// The & operator generates a pointer to its operand (i)
	p := &i

	// * denotes the underlying value
	// read from a pointer with * operand
	fmt.Printf("p has value %v, type %T\n", *p, *p)

	// set the underlying value through pointer
	*p = 24
	fmt.Printf("p has value %v, type %T\n", *p, *p)
	fmt.Printf("i has value %v, type %T\n", i, i)

	// point to j operand
	p = &j
	fmt.Printf("p value %v type %T\n", *p, *p)

	// set the underlying value of p
	*p = *p - 701
	fmt.Printf("p value %v type %T\n", *p, *p)
	fmt.Printf("j value %v type %T\n", j, j)
}

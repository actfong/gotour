package main

import (
	"fmt"
	"strings"
)

// slices can be multi dimensional
// first dimension contains slices
// second dimension contain strings
func main() {
	board := [][]string{
		[]string{"-", "-", "-"},
		[]string{"-", "-", "-"},
		[]string{"-", "-", "-"},
	}

	board[0][2] = "X"
	board[1][0] = "O"
	board[1][1] = "X"
	board[1][2] = "O"
	board[2][0] = "X"

	// strings.Join
	for i := 0; i < len(board); i++ {
		fmt.Printf("%s\n", strings.Join(board[i], " "))
	}
}

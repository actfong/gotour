package main

import (
	"fmt"
)

var pow = []int{1, 2, 4, 8, 16, 32}

// The range form of the for loop iterates over a slice or map.
// When ranging over a slice, two values are returned for each iteration.
// The first is the index, and the second is a copy of the element at that index.
func main() {
	regular(pow)
	skipIndex(pow)
	skipValue(pow)
}

func regular(slice []int) {
	for i, v := range slice {
		fmt.Printf("Index = %d, Value = %d\n", i, v)
	}
	fmt.Printf("Type = %T, Value = %v\n", slice, slice)
}

func skipIndex(slice []int) {
	fmt.Println("skipping indices")
	for _, v := range slice {
		// fmt.Printf("Index = %d, Value = %d\n", _, v)
		fmt.Printf("Value = %d\n", v)
	}
}

func skipValue(slice []int) {
	fmt.Println("skipping values")
	for i := range slice {
		// fmt.Printf("Index = %d, Value = %d\n", i, v)
		fmt.Printf("Index = %d\n", i)
	}
}

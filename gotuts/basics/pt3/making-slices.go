package main

import "fmt"

func main() {
	// length and capacity of 5. All zero-values
	var a = make([]int, 5)
	fmt.Println(a, len(a), cap(a))

	// len(b)=0, cap(b)=5
	var b = make([]int, 0, 5)
	fmt.Println(b, len(b), cap(b))

	c := b[:2]
	fmt.Println(b, len(c), cap(c))

	d := c[2:5]
	fmt.Println(b, len(d), cap(d))
}

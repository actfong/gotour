package main

import (
	"golang.org/x/tour/pic"
)

// using make is essential in this exercise
func Pic(dx, dy int) [][]uint8 {
	// for-for implementation
	row := make([][]uint8, dy)
	for i := 0; i < dy; i++ {
		column := make([]uint8, dx)
		for j := 0; j < dx; j++ {
			column[j] = uint8((i + 1) * j)
		}
		row[i] = column
	}
	return row

	// range implementation
	// x := make([][]uint8, dy)
	// for i, _ := range x {
	// 	x[i] = make([]uint8, dx)
	// 	for id := 0; id < dx; id++ {
	// 		x[i][id] = uint8((id + i) * int(dy))
	// 	}
	// }
	// return x
}

func main() {
	// Pic(2, 3)
	pic.Show(Pic)
}

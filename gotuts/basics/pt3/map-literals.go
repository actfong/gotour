package main

import "fmt"

type Country struct {
	name       string
	population int
	continent  string
}

func main() {
	var countryMap1 = map[string]Country{
		"Germany": Country{
			"Deutschland", 80000000, "Europe",
		},
		"Hong Kong": Country{
			"香港", 7800000, "Asia",
		},
	}
	fmt.Println(countryMap1)

	// Top level type can be omitted in map-literal definition
	var countryMap2 = map[string]Country{
		"Netherlands": {"Nederland", 17000000, "Europe"},
		"Malaysia":    {"Malaysia", 32000000, "Asia"},
	}
	fmt.Println(countryMap2)
}

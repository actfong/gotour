package main

import "fmt"

// List of variables of the same type can be declared using the keyword "var"
// Their values will be the default value (falsy) of their type
var c, python, java bool

// above are variables at package level
// below are variables at function level
func main() {
	var i int
	fmt.Println(i, c, python, java)
}

package main

import "fmt"
import "math"

// return values can be named,
// allowing you to use a "naked return"
func fizz(x, y int) (multi int, power float64) {
	multi = x * y
	power = math.Pow(2, 3)
	return
}

func main() {
	multi, power := fizz(2, 3)
	fmt.Println("2 and 3 yields: ", multi, power)
}

package main

import (
	"fmt"
	"math"
)

// Constants are declared like variables, but with the keyword "const".
// Constants can be character, string, boolean, or numeric values.
// Constants cannot be declared using the := syntax.

// constants can be declared at package level or at function level
const Pi = math.Pi

func main() {
	const World = "World"
	const Truth = true
	fmt.Printf("Type %T, Value %v\n", Pi, Pi)
	fmt.Printf("Type %T, Value %v\n", World, World)
	fmt.Printf("Type %T, Value %v\n", Truth, Truth)
}

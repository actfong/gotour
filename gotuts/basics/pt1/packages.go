// By convention, the package name is the same as the last element of the import path.
// For instance, the "math/rand" package comprises files that begin with the statement package rand.
package main

// when importing multiple packages, use the parenthesized syntax a.k.a "factored" import statement
import (
	"fmt"
	"math/rand"
)

func main() {
	fmt.Println("Random number is ", rand.Intn(10))
}

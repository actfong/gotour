package main

import (
	"fmt"
)

// Outside a function, every statement begins with a keyword (var, func, and so on)
// and so the := construct is not available.

// Inside a function, the := short assignment statement can be used
// in place of a var declaration with implicit type.
func main() {
	var i, j int = 0, 1
	k := 2
	c, python, java := true, false, true
	var l, m = "l", "m"
	fmt.Println(i, j)
	fmt.Println(k)
	fmt.Println(c, python, java)
	fmt.Println(l, m)
}

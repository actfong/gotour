package main

import "fmt"

// types must be declared for function parameters
// Return type is declared between the parameters and the function body
func add(x int, y int) int {
	return x + y
}

func main() {
	fmt.Println("4 + 2 = ", add(4, 2))
}

// If function parameters share a type:
// func add(x, y int) int {
// 	return x + y
// }

package main

import (
	"fmt"
	"math/cmplx"
)

// variable declarations may be "factored" into blocks, as with import statements.
var (
	booly  bool       = false
	MaxInt uint64     = 1<<64 - 1
	z      complex128 = cmplx.Sqrt(-5 + 12i)
)

// %T for type
// %v for value
func main() {
	fmt.Printf("Type %T, Value %v\n", booly, booly)
	fmt.Printf("Type %T, Value %v\n", MaxInt, MaxInt)
	fmt.Printf("Type %T, Value %v\n", z, z)
}

package main

import (
	"fmt"
)

// Variables declared without an explicit initial value are given their zero value.
// zero values are false, 0 and ""
func main() {
	var i int
	var f float64
	var b bool
	var s string

	fmt.Printf("Type %T, Value %v\n", i, i)
	fmt.Printf("Type %T, Value %v\n", f, f)
	fmt.Printf("Type %T, Value %v\n", b, b)
	fmt.Printf("Type %T, Value %v\n", s, s)
}

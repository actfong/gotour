package main

import (
	"fmt"
	"math"
)

// The expression T(v) converts the value v to the type T.
func main() {
	x, y := 3, 4
	f := math.Sqrt(float64(x*x + y*y))
	z := uint(f)

	fmt.Printf("Type %T, Value %v\n", x, x)
	fmt.Printf("Type %T, Value %v\n", f, f)
	fmt.Printf("Type %T, Value %v\n", z, z)
}

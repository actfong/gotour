package main

import (
	"fmt"
)

// A variable declaration can include initializers, one per variable.
var c, python, java bool = true, false, true

// If an initializer is present, the type can be omitted;
// the variable will take the type of the initializer.
func main() {
	var i, j = 0, 1
	fmt.Println(c, python, java, i, j)
}

package main

import "fmt"

// Multiple return values can be returned
func swap(first, last string) (string, string) {
	return last, first
}

// Function can take undefined number of function parameters
func append(args ...string) string {
	result := ""
	for _, value := range args {
		result += value + ","
	}
	return result
}

func main() {
	// comma separated variable assignment
	last, first := swap("mickey", "mouse")
	fmt.Println("swap first and last:", last, first)
	fmt.Println("Appending first and last", append("mickey", "mouse", "third name", "fourth name"))
}
